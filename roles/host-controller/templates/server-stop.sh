#!/bin/bash
#This script stops the JBoss EAP 7.x domain controller for this environment.
{% for host_ in hostnames_.keys() %}
{% if host_ == targetHost  %}
JAVA_HOME={{ jdkHome[host_] }}
PATH=${JAVA_HOME}/bin:${PATH}
JBOSS_HOME={{ jbossHome[host_] }}
DOMAIN_CONFIG={{ domainConfigName[host_] }}
HOST_CONFIG={{ hostConfigName[host_] }}
MGMT_HOST={{ mmgmtHost }}
MGMT_PORT={{ msecureMgmtPort }}
DOMAIN_HOME={{ domainHome[host_] }}
HOST_NAME={{ slaveHostName[host_] }}
SERVER_NAME=$1
{% endif %}
{% endfor %}

if [ -z "${ADMIN_USER}" ];
then
  echo -n "Enter Admin username:"
  read ADMIN_USER
fi

if [ -z "${ADMIN_PASSWORD}" ];
then
  echo -n "Enter Admin password:"
  read -s ADMIN_PASSWORD
fi

${JBOSS_HOME}/bin/jboss-cli.sh --connect \
 --controller=https-remoting://${MGMT_HOST}:${MGMT_PORT} \
 --user=${ADMIN_USER} \
 --password=${ADMIN_PASSWORD} \
 -Djavax.net.ssl.trustStore=${TRUST_STORE}\
 --command=/host=${HOST_NAME}/server-config=${SERVER_NAME}:stop

exit 0
