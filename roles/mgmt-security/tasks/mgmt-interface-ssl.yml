---
- name: Create admin keystore + cert/key
  include_role:
    name: gen-cert-wrapper
  vars:
    genCertWrapperCN: "{{ mgmtIPAddr }}"
    genCertWrapperAltServerName: ""
    genCertWrapperKeySSLPassword: "{{ mmgmtSSLKeyPassword }}"
    genCertWrapperSSLKeystorePath: "{{ mmgmtSSLKeyStorePath }}"
    genCertWrapperSSLKeyAlias: "{{ mmgmtSSLKeyAlias }}"
    genCertWrapperUseSelfSignedCerts: "{{ useSelfSignedCerts }}"
    genCertWrapperFinalFormatPEM: False
    genCertWrapperDisplayBanner: "eap-{{ mmgmtHost }}-{{ mmasterHostName }}-mgmt-cert"
    genCertWrapperSubDomain: "{{ subDomain }}"
    state: generate
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Add CA certificates to admin keystore (allows full trustchain to be presented by SSL/TLS endpoints)
  include_role:
    name: update-truststore
  vars:
    updateTruststorePath: "{{ mmgmtSSLKeyStorePath }}"
    updateTruststoreAlias: "ca"
    updateTruststorePassword: "{{ mmgmtSSLKeyPassword }}"
  register: command_output
  notify: printResults
  when: useSelfSignedCerts == False
  no_log: "{{ no_log_status }}"

- name: Change file ownership of JKS keystore file
  file:
    name: "{{ mmgmtSSLKeyStorePath }}"
    mode: 0640
    owner: "{{ mprocessUser }}"
    group: "jboss"
  register: command_output
  notify: printResults

- name: Remove properties file reference in authentication stanza of default ManagementRealm
  include_role:
    name: jboss-cli
  vars:
    jbossCliHome: "{{ mjbossHome }}"
    jbossCliTimeout: "{{ mcliTimeout }}"
    jbossCliHost: "{{  mgmtIPAddr }}"
    jbossCliPort: "{{ mmgmtPort }}"
    jbossCliAdminUser: "{{ madminADUsername }}"
    jbossCliAdminPassword: "{{ madminADPassword }}"
    jbossCliCommand: "/core-service=management/security-realm=ManagementRealm/authentication=properties:remove"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Remove properties file reference in authorization stanza of default ManagementRealm
  include_role:
    name: jboss-cli
  vars:
    jbossCliHome: "{{ mjbossHome }}"
    jbossCliTimeout: "{{ mcliTimeout }}"
    jbossCliHost: "{{  mgmtIPAddr }}"
    jbossCliPort: "{{ mmgmtPort }}"
    jbossCliAdminUser: "{{ madminADUsername }}"
    jbossCliAdminPassword: "{{ madminADPassword }}"
    jbossCliCommand: "/core-service=management/security-realm=ManagementRealm/authorization=properties:remove"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Create ManagementRealm Security Realm
  include_role:
    name: jboss-cli
  vars:
    jbossCliHome: "{{ mjbossHome }}"
    jbossCliTimeout: "{{ mcliTimeout }}"
    jbossCliHost: "{{  mgmtIPAddr }}"
    jbossCliPort: "{{ mmgmtPort }}"
    jbossCliAdminUser: "{{ madminADUsername }}"
    jbossCliAdminPassword: "{{ madminADPassword }}"
    jbossCliCommand: "/core-service=management/security-realm=ManagementRealmHTTPS/:add"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Add https management interface
  include_role:
    name: jboss-cli
  vars:
    jbossCliHome: "{{ mjbossHome }}"
    jbossCliTimeout: "{{ mcliTimeout }}"
    jbossCliHost: "{{ mgmtIPAddr }}"
    jbossCliPort: "{{ mmgmtPort }}"
    jbossCliAdminUser: "{{ madminADUsername }}"
    jbossCliAdminPassword: "{{ madminADPassword }}"
    jbossCliCommand: "/core-service=management/management-interface=http-interface:write-attribute(name=secure-socket-binding, value=management-https)"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Remove http management interface
  include_role:
    name: jboss-cli
  vars:
    jbossCliHome: "{{ mjbossHome }}"
    jbossCliTimeout: "{{ mcliTimeout }}"
    jbossCliHost: "{{ mgmtIPAddr }}"
    jbossCliPort: "{{ mmgmtPort }}"
    jbossCliAdminUser: "{{ madminADUsername }}"
    jbossCliAdminPassword: "{{ madminADPassword }}"
    jbossCliCommand: "/core-service=management/management-interface=http-interface:undefine-attribute(name=socket-binding)"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

# - name: Set admin SSL/TLS port
#   include_role:
#     name: jboss-cli
#   vars:
#     jbossCliHome: "{{ mjbossHome }}"
#     jbossCliTimeout: "{{ mcliTimeout }}"
#     jbossCliHost: "{{  mgmtIPAddr }}"
#     jbossCliPort: "{{ mmgmtPort }}"
#     jbossCliAdminUser: "{{ madminADUsername }}"
#     jbossCliAdminPassword: "{{ madminADPassword }}"
#     jbossCliCommand: "/core-service=management/management-interface=http-interface:write-attribute(name=secure-port,value=9993)"
#   register: command_output
#   notify: printResults
#   no_log: "{{ no_log_status }}"

# - name: Delete definition for non-SSL port
#   include_role:
#     name: jboss-cli
#   vars:
#     jbossCliHome: "{{ mjbossHome }}"
#     jbossCliTimeout: "{{ mcliTimeout }}"
#     jbossCliHost: "{{  mgmtIPAddr }}"
#     jbossCliPort: "{{ mmgmtPort }}"
#     jbossCliAdminUser: "{{ madminADUsername }}"
#     jbossCliAdminPassword: "{{ madminADPassword }}"
#     jbossCliCommand: "/core-service=management/management-interface=http-interface:undefine-attribute(name=port)"
#   register: command_output
#   notify: printResults
#   no_log: "{{ no_log_status }}"

- name: "Add mgmt interface keystore to password vault."
  include_role:
    name: add-password-2-vault
  vars:
    addPasswordJBossHome: "{{ mjbossHome }}"
    addPasswordVaultEncDir: "{{ mvaultEncDir }}"
    addPasswordVaultSaltIteration: "{{ mvaultSaltIteration }}"
    addPasswordVaultKeystore: "{{ mvaultKeystore }}"
    addPasswordVaultPassword: "{{ mvaultPassword }}"
    addPasswordVaultSalt: "{{ mvaultSalt }}"
    addPasswordVaultKeystoreAlias: "{{ mvaultKeystoreAlias }}"
    addPasswordKey: "mgmtKeystorePassword"
    addPasswordValue: "{{ mmgmtSSLKeyPassword }}"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

- name: Set keystore configuration for management interface
  include_role:
    name: jboss-cli
  vars:
    jbossCliHome: "{{ mjbossHome }}"
    jbossCliTimeout: "{{ mcliTimeout }}"
    jbossCliHost: "{{ mgmtIPAddr }}"
    jbossCliPort: "{{ mmgmtPort }}"
    jbossCliAdminUser: "{{ madminADUsername }}"
    jbossCliAdminPassword: "{{ madminADPassword }}"
    jbossCliCommand: "/core-service=management/security-realm=ManagementRealmHTTPS/server-identity=ssl:add(keystore-path={{ mmgmtSSLKeyStorePath }},keystore-password=${VAULT::vb::mgmtKeystorePassword::1}, alias={{ mmgmtSSLKeyAlias }})"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

# Add local interface definition
# - name: Add local interface definition
#   include_role:
#     name: jboss-cli
#   vars:
#     jbossCliHome: "{{ mjbossHome }}"
#     jbossCliTimeout: "{{ mcliTimeout }}"
#     jbossCliHost: "{{  mgmtIPAddr }}"
#     jbossCliPort: "{{ mmgmtPort }}"
#     jbossCliAdminUser: "{{ madminADUsername }}"
#     jbossCliAdminPassword: "{{ madminADPassword }}"
#     jbossCliCommand: "/interface=local-management:add(inet-address=localhost)"
#   register: command_output
#   notify: printResults
#   no_log: "{{ no_log_status }}"

# - name: Point native-interface Management Interface at local-management interface
#   include_role:
#     name: jboss-cli
#   vars:
#     jbossCliHome: "{{ mjbossHome }}"
#     jbossCliTimeout: "{{ mcliTimeout }}"
#     jbossCliHost: "{{  mgmtIPAddr }}"
#     jbossCliPort: "{{ mmgmtPort }}"
#     jbossCliAdminUser: "{{ madminADUsername }}"
#     jbossCliAdminPassword: "{{ madminADPassword }}"
#     jbossCliCommand: "/core-service=management/management-interface=native-interface:write-attribute(name=interface,value=local-management)"
#   register: command_output
#   notify: printResults
#   no_log: "{{ no_log_status }}"

# - name: Add Native Management Interface for BMC Patrol
#   include_role:
#     name: jboss-cli
#   vars:
#     jbossCliHome: "{{ mjbossHome }}"
#     jbossCliTimeout: "{{ mcliTimeout }}"
#     jbossCliHost: "{{  mgmtIPAddr }}"
#     jbossCliPort: "{{ mmgmtPort }}"
#     jbossCliAdminUser: "{{ madminADUsername }}"
#     jbossCliAdminPassword: "{{ madminADPassword }}"
#     jbossCliCommand: "batch, /socket-binding-group=standard-sockets/socket-binding=management-native:add, /socket-binding-group=standard-sockets/socket-binding=management-native:write-attribute(name=interface, value=management), /socket-binding-group=standard-sockets/socket-binding=management-native:write-attribute(name=port, value=${jboss.management.native.port:9999}), /core-service=management/management-interface=native-interface:add(security-realm=ManagementRealm, socket-binding=management-native), run-batch"
#     jbossCliBatch: True
#   register: command_output
#   notify: printResults
#   no_log: "{{ no_log_status }}"

- name: "Add host element to jboss-cli.xml."
  xml:
    path: "{{ mjbossHome }}/bin/jboss-cli.xml"
    xpath: /xmlns:jboss-cli/xmlns:default-controller/xmlns:host
    namespaces:
      xmlns: "{{ jbossCliNamespace }}"
    ensure: present
    value: "{{ mgmtIPAddr }}"
  become_user: root
  register: command_output
  notify: printResults

- name: "Add port element to jboss-cli.xml."
  xml:
    path: "{{ mjbossHome }}/bin/jboss-cli.xml"
    xpath: /xmlns:jboss-cli/xmlns:default-controller/xmlns:port
    namespaces:
      xmlns: "{{ jbossCliNamespace }}"
    ensure: present
    value: "{{ msecureMgmtPort }}"
  become_user: root
  register: command_output
  notify: printResults

- name: "Add protocol eleement to jboss-cli.xml."
  xml:
    path: "{{ mjbossHome }}/bin/jboss-cli.xml"
    xpath: /xmlns:jboss-cli/xmlns:default-controller/xmlns:protocol
    namespaces:
      xmlns: "{{ jbossCliNamespace }}"
    ensure: present
    value: https-remoting
  become_user: root
  register: command_output
  notify: printResults

- name: "Add ssl element to jboss-cli.xml."
  xml:
    path: "{{ mjbossHome }}/bin/jboss-cli.xml"
    xpath: /xmlns:jboss-cli/xmlns:ssl
    namespaces:
      xmlns: "{{ jbossCliNamespace }}"
    ensure: present
  become_user: root
  register: command_output
  notify: printResults

- name: "Add ssl/trust-store element to jboss-cli.xml."
  xml:
    path: "{{ mjbossHome }}/bin/jboss-cli.xml"
    xpath: /xmlns:jboss-cli/xmlns:ssl/xmlns:trust-store
    namespaces:
      xmlns: "{{ jbossCliNamespace }}"
    ensure: present
    value: "{{ mmasterTrustStorePath }}"
  become_user: root
  register: command_output
  notify: printResults

- name: "Add ssl/trust-store-password eleement to jboss-cli.xml."
  xml:
    path: "{{ mjbossHome }}/bin/jboss-cli.xml"
    xpath: /xmlns:jboss-cli/xmlns:ssl/xmlns:trust-store-password
    namespaces:
      xmlns: "{{ jbossCliNamespace }}"
    ensure: present
    value: "${VAULT::vb::mgmtTrustStorePassword::1}"
  become_user: root
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"

# - name: "Add ssl/trust-store eleement to jboss-cli.xml."
#   xml:
#     path: "{{ mjbossHome }}/bin/jboss-cli.xml"
#     xpath: /xmlns:jboss-cli/xmlns:ssl/xmlns:trust-store
#     namespaces:
#      xmlns: "{{ jbossCliNamespace }}"
#     ensure: present
#     value: ""
#   become_user: root
#   register: command_output
#   notify: printResults

- name: "Add host truststore password to password vault."
  include_role:
    name: add-password-2-vault
  vars:
    addPasswordJBossHome: "{{ mjbossHome }}"
    addPasswordVaultEncDir: "{{ mvaultEncDir }}"
    addPasswordVaultSaltIteration: "{{ mvaultSaltIteration }}"
    addPasswordVaultKeystore: "{{ mvaultKeystore }}"
    addPasswordVaultPassword: "{{ mvaultPassword }}"
    addPasswordVaultSalt: "{{ mvaultSalt }}"
    addPasswordVaultKeystoreAlias: "{{ mvaultKeystoreAlias }}"
    addPasswordKey: "mgmtTrustStorePassword"
    addPasswordValue: "{{ mmasterTrustStorePassword }}"
  register: command_output
  notify: printResults
  no_log: "{{ no_log_status }}"
