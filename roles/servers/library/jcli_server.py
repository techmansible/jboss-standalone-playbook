#!/usr/bin/python

from ansible.module_utils.basic import *
import subprocess
import time

def init(data):
    cmd = data['jboss_home'] + '/bin/jboss-cli.sh'
    exists, created, alreadyCreatedResult,alreadyCreatedErr = isAlreadyDone(data)
    controller = "--controller=https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
    timeout = "--timeout=%s" % (data['timeout'])
    user = "-u=%s" % (data['user'])
    password = "-p=%s" % (data['password'])
    if data['masterTrustStorePath'] != None:
      trustStore = "-Djavax.net.ssl.trustStore=%s" % data['masterTrustStorePath']
    else:
      trustStore = "-Dnothing"
    isError = False
    hasChanged = True
    meta = {}
    return cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout

def checkError(result):
    if "=> \"success\"" in result:
        return False #No error
    else:
        return True #Error

def checkCreated(result):
    if "=> \"success\"" in result:
        return True #Object created
    else:
        return False #Object not created

def makeCall(commandArgs):
    p = subprocess.Popen(commandArgs, stdout=subprocess.PIPE)
    result , err = p.communicate()
    created = False
    remoteExists = False

    if "WFLYPRT0053" in result:
        remoteExists = False
    else:
        remoteExists = True
    created = checkCreated(result)
    return remoteExists, created,result,err

def makeCall1(commandArgs):
    p = subprocess.Popen(commandArgs, stdout=subprocess.PIPE)
    result,err = p.communicate()
    isError = checkError(result)
    return isError,result,err

def buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError):
    return {
		"status": "OK", 
		"response": result, 
		"error":err, 
		"checkResponse": alreadyCreatedResult, 
		"checkErr": alreadyCreatedErr,
                "hasChanged": hasChanged,
                "isError": isError
           }
def isAlreadyDone(data):
    cmd = data['jboss_home'] + '/bin/jboss-cli.sh'
    cli = "/host=%s/server=%s:query" % (data['host'], data['server_config_name'])
    controller = "--controller=https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
    timeout= "--timeout=%s" % (data['timeout'])
    user = "-u=%s" % (data['user'])
    password = "-p=%s" % (data['password'])
    if data['masterTrustStorePath'] != None:
      trustStore = "-Djavax.net.ssl.trustStore=%s" % data['masterTrustStorePath']
    else:
      trustStore = "-Dnothing"
    args = ["sh", cmd, "-c", cli, controller, user, password, trustStore]
    return makeCall(args)

def server_present(data):
    cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = buildResponse(mesg, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        isError = True
        hasChanged = False
    else:
        if not created:
            cli = "/host=%s/server-config=%s:add(group=%s,socket-binding-port-offset=%s,socket-binding-group=%s)" % (data['host'],data['server_config_name'],data['server_group_name'],data['server_socket_binding_port_offset'],data['server_group_socket'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)
            if isError == False:
              hasChanged = True
            meta = buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        else:
            hasChanged = False
            isError = False
            resp = "Server %s already created" % (data['server_config_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
    return isError, hasChanged, meta

def server_absent(data):
    cmd,exists,created,controller,alreadyCreatedResult,alreadyCreatedErr,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    res = ""
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = buildResponse(mesg, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        isError = True
        hasChanged = False
    else:
        if not created:
            hasChanged = False
            resp = "Server %s does not exist" % (data['server_config_name'])
            meta = buildResponse({}, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
        else:
            cli = "/host=%s/server-config=%s:stop" % (data['host'],data['server_config_name'])
            p = subprocess.Popen(["sh", cmd, "-c", cli, controller, user, password], stdout=subprocess.PIPE)
            result,err = p.communicate()
            res.append(cli + "|" + result)
            result = str(result)

            while not "STOPPED" in result:
                time.sleep(0.5)
                cli = "/host=%s/server-config=%s:stop" % (data['host'],data['server_config_name'])
                p = subprocess.Popen(["sh", cmd, "-c", cli, controller, user, password], stdout=subprocess.PIPE)
                result = p.communicate()[0]
                result = str(result,'utf-8')

            cli = "/host=%s/server-config=%s:remove" % (data['host'],data['server_config_name'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs);
            res.append(cli + "|" + result)
            meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
    return isError, hasChanged, meta

def server_start(data):
    cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout = init(data) 
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = {"status": "Error", "response": mesg, "checkResponse": alreadyCreatedResult}
        isError = True
        hasChanged = False
    else:
        if created:
            cli = "/host=%s/server-config=%s:start" % (data['host'],data['server_config_name'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs);
            meta = buildResponse(cli + "|" +result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
        else:
            hasChanged = False
            isError = True
            resp = "Server %s does not exist" % (data['server_config_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
    return isError, hasChanged, meta

def server_stop(data):
    cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = {"status": "Error", "response": mesg}
        isError = True
        hasChanged = False
    else:
        if created:
            cli = "/host=%s/server-config=%s:stop" % (data['host'],data['server_config_name'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs);
            meta = buildResponse(cli + "|" + result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        else:
            hasChanged = False
            isError = True
            resp = "Server %s does not exist" % (data['server_config_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)

    return isError, hasChanged, meta

def main():
 
    fields = {
        "jboss_home" : {"required": True, "type": "str"},
        "host": {
            "required": False,
            "default": "master",
            "type": "str"
        },
        "server_group_name": {
            "required": True,
            "type": "str"
        },
        "server_config_name": {
            "required": True,
            "type": "str"
        },
        "server_socket_binding_port_offset": {
            "required": False,
            "default": 0,
            "type": "int"
        },
        "server_group_socket": {
            "required": False,
            "default": "standard-sockets",
            "type": "str"
        },
        "controller_host": {
            "required": False,
            "default": "localhost",
            "type": "str"
        },
        "controller_port": {
            "required": False,
            "default": 9990,
            "type": "int"
        },
        "user" : {
            "required": True,
            "type": "str"
        },
        "password" : {
            "required": True,
            "type": "str"
        },
        "state": {
            "default": "present",
            "choices": ['present', 'absent', 'start', 'stop'],
            "type": 'str'
        },
        "trustStorePath": {
            "required": False,
            "type": 'str',
            "default":""
        },
        "masterTrustStorePath":  {
            "required": False,
            "type": 'str',
            "default":""
        },

        "timeout": {
            "required": False,
            "type": 'str',
            "default":"30000"
        }
    }
 
    choice_map = {
        "present": server_present,
        "absent": server_absent,
        "start": server_start,
        "stop": server_stop,
    }
 
    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result = choice_map.get(
        module.params['state'])(module.params)
 
    if not is_error:
        module.exit_json(changed=has_changed, meta=result)
    else:
        module.fail_json(msg="Error creating server", meta=result)


if __name__ == '__main__':
    main()
