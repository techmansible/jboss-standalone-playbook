#!/usr/bin/python

from ansible.module_utils.basic import *
import subprocess

def init(data):
    cmd = data['jboss_home'] + '/bin/jboss-cli.sh'
    exists, created, alreadyCreatedResult,alreadyCreatedErr = isAlreadyDone(data)
    controller = "--controller=https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
    timeout="--timeout=%s" % (data['timeout'])
    user = "-u=%s" % (data['user'])
    password = "-p=%s" % (data['password'])
    if data['masterTrustStorePath'] != None:
      trustStore = "-Djavax.net.ssl.trustStore=%s" % data['masterTrustStorePath']
    else:
      trustStore = "-Dnothing"
    isError = False
    hasChanged = True
    meta = {}
    return cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout

def checkError(result):
    if "=> \"success\"" in result:
        return False #No error
    else:
        return True #Error

def checkCreated(result):
    if "=> \"success\"" in result:
        return True #Object created
    else:
        return False #Object not created

def makeCall(commandArgs):
    p = subprocess.Popen(commandArgs, stdout=subprocess.PIPE)
    result , err = p.communicate()
    created = False
    remoteExists = False

    if "WFLYPRT0053" in result:
        remoteExists = False
    else:
        remoteExists = True
    created = checkCreated(result)
    return remoteExists, created,result,err

def makeCall1(commandArgs):
    p = subprocess.Popen(commandArgs, stdout=subprocess.PIPE)
    result,err = p.communicate()
    isError = checkError(result)
    return isError,result,err

def buildResponse(result, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError):
    return {
		"status": "OK", 
		"response": result, 
		"error":err, 
		"checkResponse": alreadyCreatedResult, 
		"checkErr": alreadyCreatedErr,
                "hasChanged": hasChanged,
                "isError": isError
           }
def isAlreadyDone(data):
    cmd = data['jboss_home'] + '/bin/jboss-cli.sh'
    cli = "/host=%s/server-config=%s/jvm=%s:query" % (data['host'], data['server_config_name'], data['jvn_name'])
    controller = "--controller=https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
    user = "-u=%s" % (data['user'])
    password = "-p=%s" % (data['password'])
    if data['masterTrustStorePath'] != None:
      trustStore = "-Djavax.net.ssl.trustStore=%s" % data['masterTrustStorePath']
    else:
      trustStore = "-Dnothing"
    args = ["sh", cmd, "-c", cli, controller, user, password,trustStore]
    return makeCall(args)

def jvm_present(data):
    cmd,exists,created,alreadyCreatedResult,alreadyCreatedErr,controller,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    res =[]
    if not exists:
        mesg = "Could not connect http-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = buildResponse(mesg, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        isError = True
        hasChanged = False
    else:
        if not created:
            cli = "/host=%s/server-config=%s/jvm=%s:add" % (data['host'],data['server_config_name'],data['jvn_name'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)
            res.append(cli + "|" + result)
            if isError == False:
              hasChanged = True
            else:
              meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
              return isError, hasChanged, meta

            cli = "/host=%s/server-config=%s/jvm=%s:write-attribute(name=heap-size,value=%s)" % (data['host'],data['server_config_name'],data['jvn_name'],data['heap_size'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)
            res.append(cli + "|" + result)
            if isError == False:
              hasChanged = True
            else:
              meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
              return isError, hasChanged, meta

            cli = "/host=%s/server-config=%s/jvm=%s:write-attribute(name=max-heap-size,value=%s)" % (data['host'],data['server_config_name'],data['jvn_name'],data['max_heap_size'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)
            res.append(cli + "|" + result)
            if isError == False:
              hasChanged = True
            else:
              meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
              return isError, hasChanged, meta

            cli = "/host=%s/server-config=%s/jvm=%s:write-attribute(name=permgen-size,value=%s)" % (data['host'],data['server_config_name'],data['jvn_name'],data['permgen_size'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)
            res.append(cli + "|" + result)
            if isError == False:
              hasChanged = True
            else:
              meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
              return isError, hasChanged, meta

            cli = "/host=%s/server-config=%s/jvm=%s:write-attribute(name=max-permgen-size,value=%s)" % (data['host'],data['server_config_name'],data['jvn_name'],data['max_permgen_size'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)
            res.append(cli + "|" + result)
            if isError == False:
              hasChanged = True
            else:
              meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
              return isError, hasChanged, meta

            if data['jvm_options'] is not None and data['jvm_options'] != "":
                cli = "/host=%s/server-config=%s/jvm=%s:add-jvm-option(jvm-option=\"%s\")" % (data['host'],data['server_config_name'],data['jvn_name'],data['jvm_options'])
                commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
                isError,result,err = makeCall1(commandArgs)
                res.append(cli + "|" + result)
                if isError == False:
                  hasChanged = True
                else:
                  meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
                  return isError, hasChanged, meta

#            cli = "reload --host=%s" % (data['host'])
#            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
#            isError,result,err = makeCall1(commandArgs)
#            res.append("".join(commandArgs) + "|" + result)
#            if isError == False:
#              hasChanged = True
#            else:
#              meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
#              return isError, hasChanged, meta

            meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
        else:
            hasChanged = False
            resp = "JVM %s already created" % (data['jvn_name'])
            meta = buildResponse(resp, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged, isError)
    return isError, hasChanged, meta

def jvm_absent(data):
    cmd,exists,created,controller,alreadyCreatedResult,alreadyCreatedErr,user,password,trustStore,isError,hasChanged, meta,timeout = init(data)
    if not exists:
        mesg = "Could not connect https-remoting://%s:%s" % (data['controller_host'],data['controller_port'])
        meta = buildResponse(mesg, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        isError = True
        hasChanged = False
    else:
        if not created:
            hasChanged = False
            resp = "JVM %s does not exist" % (data['jvn_name'])
            meta = buildResponse(resp, {}, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
        else:
            cli = "/host=%s/server-config=%s/jvm=%s:remove" % (data['host'],data['server_config_name'],data['jvn_name'])
            commandArgs = ["sh", cmd, "-c", cli, controller, user, password,trustStore,timeout]
            isError,result,err = makeCall1(commandArgs)
            res.append(cli + "|" + result)
            if isError == False:
              hasChanged = True
            meta = buildResponse(res, err, alreadyCreatedResult, alreadyCreatedErr,hasChanged,isError)
    return isError, hasChanged, meta

def main():

    fields = {
        "jboss_home" : {"required": True, "type": "str"},
        "host": {
            "required": False,
            "default": "master",
            "type": "str"
        },
        "controller_host": {
            "required": False,
            "default": "localhost",
            "type": "str"
        },
        "controller_port": {
            "required": False,
            "default": 9990,
            "type": "int"
        },
        "server_config_name": {
            "required": True,
            "type": "str"
        },
        "jvn_name": {
            "required": True,
            "type": "str"
        },
        "heap_size": {
            "required": True,
            "type": "str"
        },
        "max_heap_size": {
            "required": True,
            "type": "str"
        },
        "permgen_size": {
            "required": True,
            "type": "str"
        },
        "max_permgen_size": {
            "required": True,
            "type": "str"
        },
        "jvm_options": {
            "required": False,
            "type": "str"
        },
        "user" : {
            "required": True,
            "type": "str"
        },
        "password" : {
            "required": True,
            "type": "str"
        },
        "state": {
            "default": "present",
            "choices": ['present', 'absent'],
            "type": 'str'
        },
        "trustStorePath": {
            "required": False,
            "type": 'str',
            "default":""
        },
        "masterTrustStorePath": {
            "required": False,
            "type": 'str',
            "default":""
        },
        "timeout": {
            "required": False,
            "type": 'str',
            "default":"30000"
        }
    }

    choice_map = {
        "present": jvm_present,
        "absent": jvm_absent,
    }

    module = AnsibleModule(argument_spec=fields)
    is_error, has_changed, result = choice_map.get(
        module.params['state'])(module.params)

    if not is_error:
        module.exit_json(changed=has_changed, meta=result)
    else:
        module.fail_json(msg="Error creating JVM", meta=result)

if __name__ == '__main__':
    main()
