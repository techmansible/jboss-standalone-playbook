#!/bin/bash
#This script starts the JBoss EAP 7.x domain controller for this environment.
JAVA_HOME={{ mjdkHome }}
PATH=${JAVA_HOME}/bin:${PATH}
JBOSS_HOME={{ mjbossHome }}
DOMAIN_CONFIG={{ mdomainConfigName }}
HOST_CONFIG={{ mhostConfigName }}
MGMT_HOST={{ mmgmtHost }}
DOMAIN_HOME={{ mdomainHome }}
TRUST_STORE={{ mmasterTrustStorePath }}
STDOUT_LOG={{ mmgmtStandardErrLog }}
STDERR_LOG={{ mmgmtStandardOutLog }}
export JAVA_OPTS="-Xms1024m -Xmx1024m -XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true -Djboss.modules.system.pkgs=org.jboss.byteman -Djava.awt.headless=true -verbose:gc -Xloggc:/opt/jboss/{{ mdomainHome }}/{{ mmgmtHost }}/domain/log/gc.log -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=5 -XX:GCLogFileSize=3M -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/jboss/{{ mdomainHome }}/{{ mmgmtHost }}/domain/log"
nohup ${JBOSS_HOME}/bin/domain.sh \
 --domain-config=${DOMAIN_CONFIG} \
 --host-config=${HOST_CONFIG} \
 -b ${MGMT_HOST} -bmanagement ${MGMT_HOST} \
 -Djboss.domain.base.dir=${DOMAIN_HOME} \
 -Djavax.net.ssl.trustStore=${TRUST_STORE} \
 -secmgr -DDomainManager \
 2>${STDERR_LOG} > ${STDOUT_LOG} &
 
exit 0
