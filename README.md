JBoss EAP Standalone Installation
---------------------------------

Ansible playbook to install jboss eap 7.1 standalone

Running Playbook Example
------------------------

ansible-playbook -i inventory -e "@vars/ev-template.yml" -e "@cyberark-config.yml" -e "state=present" master-playbook.yml -v 

note: use ev-template.yml as a reference only
